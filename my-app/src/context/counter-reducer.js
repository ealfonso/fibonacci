export const reducer = (state, action) => {
    switch(action.type) {
        case "CALCULATE_FIBONACCI": 
            return {
                ...state,
                number: action.payload
            }
    }
}

export const initialState = {
    number: 0
}
