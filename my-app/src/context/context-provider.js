import React from 'react'
import { reducer, initialState } from "./counter-reducer"

export const NumberContext = React.createContext({
    state: initialState,
    dispatch: () => null
  })
  
  export const UserProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(reducer, initialState)
      return (
        <NumberContext.Provider value={[ state, dispatch ]}>
          { children }
        </NumberContext.Provider>
      )
    }