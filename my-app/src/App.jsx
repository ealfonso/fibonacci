import logo from './logo.svg'
import './App.css';
import Button from '@mui/material/Button';
import { useNavigate } from "react-router-dom";

const App = (props) => {
  const navigate = useNavigate();

  return (
   
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Click "Start" button to Fibonacci counter
        </p>
        <Button variant="contained" onClick = {() => navigate("/fibonacci", { replace: true })}>Start</Button>
      </header>
    </div>
  );
}

export default App;
