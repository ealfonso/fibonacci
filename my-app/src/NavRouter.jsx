import React from "react";
import {
    BrowserRouter as Router,
    Routes,
    Route,
  } from "react-router-dom";
import App from "./App";
import { Counter } from "./counter/counter-component";

export const NavRouter = () => {
    return (
    <Router>
    <Routes>
        <Route path="/" element={<App/>} />
        <Route path="/fibonacci" element={   <Counter />}/>
    </Routes>
    </Router>
    )
}