import { Button, TextField, Item } from "@mui/material";
import { Box } from "@mui/system";
import React, { Fragment, useState, useEffect } from "react";
import { NumberContext } from "../context/context-provider";


export const Counter = () => {
    const [ state, dispatch ] = React.useContext(NumberContext)
    const [ number, setNumber ] = useState(0)
    const [finalValue, setFinalValue] = useState('0')

    const handleClick = () => {
        dispatch({type: "CALCULATE_FIBONACCI", payload: number})
        console.log("entro")
        fetch('http://localhost:3001/api/fibonacci/' + number)
          .then((response) => response.json())
          .then(data => setFinalValue(data));
    }

    const handleChange = (event) => {
        setNumber(event.target.value)
    }

    useEffect(() => {
        
      }, []);
    

    return (
        <Fragment>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                }}>
                <h1>Calculate Fibonacci !</h1>
            </Box>
            <Box
            component="form"
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    '& > :not(style)': { m: 1, width: '25ch' },
                  }}>
                <TextField
                    id="outlined-name"
                    label="Number"
                    type="number"
                    value={number}
                    onChange={handleChange}
                />
                <Button variant="contained" onClick = {handleClick}>Calculate</Button>
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                }}>
            <h3>Fibonacci result : {finalValue}</h3>
            </Box>
        </Fragment>

    )
}
